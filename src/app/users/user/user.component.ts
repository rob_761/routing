import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user: {id: number, name: string};
  paramsSubscription: Subscription;

  constructor(private route: ActivatedRoute) { } //currently loaded route contains the id & name

  ngOnInit() {
    this.user = {
      id: this.route.snapshot.params['id'] , //defined in route parameters
      name:this.route.snapshot.params['name']
    };
    this.paramsSubscription = this.route.params.subscribe( //fired EVERY time params change
      (params: Params) => { //es6 arrow function
        this.user.id = params['id'];
        this.user.name = params['name'];
      }
    );
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

}
