import {NgModule} from "@angular/core";
import {ServerComponent} from "./servers/server/server.component";
import {ServersComponent} from "./servers/servers.component";
import {HomeComponent} from "./home/home.component";
import {UserComponent} from "./users/user/user.component";
import {UsersComponent} from "./users/users.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {EditServerComponent} from "./servers/edit-server/edit-server.component";
import {Routes, RouterModule} from "@angular/router";
import {AuthGuardService} from "./auth-guard.service";
import {CanDeactivateGuardService} from "./servers/edit-server/can-deactivate-guard.service";
import {ErrorPageComponent} from "./error-page/error-page.component";
import {ServerResolverService} from "./servers/server/server-resolver.service";


const appRoutes: Routes = [
  { path: '', component: HomeComponent},   //empty path
  { path: 'users', component: UsersComponent,
    children: [
      { path: ':id/:name', component: UserComponent},  //create dynamic path by adding a parameter to path
    ]},
  { path: 'servers', component: ServersComponent,
    //canActivate:[AuthGuardService], //protects servers and child routes
    canActivateChild:[AuthGuardService], //protects child routes only
    children: [
      {path: ':id', component: ServerComponent, resolve: {server: ServerResolverService}},
      {path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuardService]},
    ]},
  //{path: 'not-found', component: PageNotFoundComponent},
  {path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found'}},
  {path: '**', redirectTo: '/not-found'}, //must be last, paths are passed top to bottom
];

@NgModule({
imports: [
  RouterModule.forRoot(appRoutes)
  // RouterModule.forRoot(appRoutes, {useHash: true}) //work with old server or browser
],
  exports: [RouterModule] //let it be accessible to components that import
})

export class AppRoutingModule {
}
