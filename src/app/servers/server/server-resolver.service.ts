import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {ServersService} from "../servers.service";
import {Injectable} from "@angular/core";

//could be own file
interface Server {
  id: number;
  name: string;
  status: string;
}

@Injectable()
export class ServerResolverService implements Resolve<{id: number, name:string, status: string}> { //wraps data field

  constructor(private serversService: ServersService){}

  //loads data in advance
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Server> | Promise<Server> | Server {
  return this.serversService.getServer(+route.params['id']);
  }
}
